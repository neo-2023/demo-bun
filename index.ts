import express from 'express';

const app = express();
const port = 8080;
const routes = require('./routes')

app.use('/admin', routes.admin);
app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`Listening on port ${port} ...`);

});
