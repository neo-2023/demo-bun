const router = require('express').Router();
const moment = require('moment');

router.use((req, res, next) => {
    console.log('Request time: ', moment(Date.now()).format('LLL'));
    next();
});

router.get('/', (req, res) => {
    res.send('Admin Response');
});

module.exports = router;